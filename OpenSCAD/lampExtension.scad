$fn=50;
sealingRubberDiameter=2;
sealingBodyHeight=4;
mainHeight=9 + sealingBodyHeight;

outerDiameter=92;
innerDiameter=63;
glassDiameter=72;

screwDiameter=4.1; //M4 screw + some wiggle room
screwDistance=outerDiameter/2-5;

sealingWidth=4;

// lower sealing part
difference() {
    cylinder(h=sealingBodyHeight,d=innerDiameter+sealingWidth*2);
    cylinder(h=sealingBodyHeight,d=innerDiameter);
}

difference(){
    // main part
    translate([0,0,sealingBodyHeight])
    difference() {
        cylinder($fn = 200, h=mainHeight,d=outerDiameter);
        cylinder(h=mainHeight,d=innerDiameter);
        
        translate([screwDistance,0,0])
            cylinder(h=mainHeight*2,d=screwDiameter);
        
        translate([-screwDistance,0,0])
            cylinder(h=mainHeight*2,d=screwDiameter);
        
        translate([0,screwDistance,0])
            cylinder(h=mainHeight*2,d=screwDiameter);
        
        translate([0,-screwDistance,0])
            cylinder(h=mainHeight*2,d=screwDiameter);
    }

    // upper sealing part
    translate([0,0,mainHeight+0.7])
    difference() {
        cylinder(h=sealingBodyHeight-0.6,d=glassDiameter);
        cylinder(h=sealingBodyHeight-0.6,d=innerDiameter);
    }

    //sealing rubber
    rotate_extrude(convexity = 10)
    translate([33.5, 13.7, 0])
    circle(d = sealingRubberDiameter);
}

